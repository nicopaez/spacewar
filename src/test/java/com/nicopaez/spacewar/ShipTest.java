package com.nicopaez.spacewar;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ShipTest {

	@Test
	public void whenCollideAsteroidLifeGetZero(){
        SpaceObject ship = new Ship();
        SpaceObject asteroid = new Asteroid();

        ship.collideWith(asteroid);

        assertEquals(0, ship.getLife());
	}


    @Test
    public void whenCollideStationSpeedGetsZero(){
        SpaceObject ship = new Ship();
        SpaceObject asteroid = new Station();

        ship.collideWith(asteroid);

        assertEquals(10, ship.getLife());
        assertEquals(0, ship.getSpeed());
    }

}
