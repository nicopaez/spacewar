package com.nicopaez.spacewar;

/**
 * Created by nicopaez on 10/31/16.
 */
public abstract class SpaceObject {

    protected int life;
    protected int speed;

    public abstract void collideWith(SpaceObject otherObject);

    public int getLife() {
        return life;
    }

    public int getSpeed() {
        return speed;
    }
}
