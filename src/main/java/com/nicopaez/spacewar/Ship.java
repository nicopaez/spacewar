package com.nicopaez.spacewar;

/**
 * Created by nicopaez on 10/31/16.
 */
public class Ship extends SpaceObject {

    public Ship() {
        this.life = 10;
        this.speed = 10;
    }
    @Override
    public void collideWith(SpaceObject otherObject) {

        if (otherObject.getClass() == Asteroid.class) {
            this.life = 0;
        }

        if (otherObject.getClass() == Station.class) {
            if (this.speed < 11){
                this.speed = 0;
            }
            else{
                this.life = 0;
            }
        }
    }
}
